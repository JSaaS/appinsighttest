﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.ApplicationInsights;

namespace AppInsightTest
{
    class Program
    {  
        static void Main()
        {
            TelemetryClient telemetry = new TelemetryClient();
            int[] array;
            int index = 4;
            array = new int[2];
            //Dictionary to hold MessageID
            var properties = new Dictionary<string, string>();
            //Dictionary to hold sequence number
            var sequence = new Dictionary<string, double>();
            #region for-loop
            for (double i = 0; i <= 1000; i++)
            {
                
                string ID = Guid.NewGuid().ToString();
                //System.Diagnostics.Trace.TraceInformation("Test!");
                properties["MessageID"] = ID;
                sequence["SequenceNumber"] = i;
                telemetry.TrackEvent("Message "+ID+" received", properties, sequence);
                telemetry.TrackMetric("BusinessPartMetric", i);
                telemetry.TrackTrace("BusinesspartTrace: " + ID);
                
               try
                {
                    bool isLetter = !String.IsNullOrEmpty(ID) && Char.IsLetter(ID[0]);
                    if (isLetter)
                    {
                        System.DataMisalignedException err = new DataMisalignedException("Can't have MessageID that begins with letters! "+ID);
                        throw err;
                    }
                }
                catch(System.DataMisalignedException ex)
                {
                    telemetry.TrackException(ex, properties, sequence);
                }

                Console.WriteLine("Test! Nu bör det finnas något i App Insight... " + ID);
                telemetry.Flush();
                
            }
            #endregion
            //Console.ReadLine();
        }
    }
}
