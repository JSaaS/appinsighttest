Detta är ett första försök med Application Insight. Koden skapar upp ett Web Job som har en Instrumentation Key till min MSDN-Azure och den Application Insight-instans jag har där. Om denna kod ska köras på annan subscription så ska den nyckeln bytas. Den ligger överst i filen ApplicationInsights.config.

Detta värde kan även sättas programmatiskt direkt i koden. 

TelemetryConfiguration.Active.InstrumentationKey = "the_key";

TelemetryConfiguration.Active.TelemetryChannel.DeveloperMode = true;

Kan vara en bättre väg att ha det så, då man kan lägga nyckeln i Web Jobets app.config istället och därmed slipper undan att hantera nyckeln i GIT öht.

Koden loopar i en FOR-loop 1001 ggr och skapar upp nya MessageID (GUID). Om ett MessageID börjar på en bokstav så kastas ett exception av typen DataMisalignedException. Detta loggas givetvis till Application Insights det med.